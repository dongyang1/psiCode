package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

const respTmpl = `PSI 24H - National: {{ .PSI24.National }}, North: {{ .PSI24.North }}, East: {{ .PSI24.East }}, South: {{ .PSI24.South }}, West: {{ .PSI24.West }}
(1) PM10 National is {{ .PM10.National }}
(2) PM10 North is {{ .PM10.North }}
(3) PM10 East is {{ .PM10.East }}
(4) PM10 South is {{ .PM10.South }}
(5) PM10 West is {{ .PM10.West }}
(6) PSI National is {{ .PSI24.National }}
(7) PSI North is {{ .PSI24.North }}
(8) PSI East is {{ .PSI24.East }}
(9) PSI South is {{ .PSI24.South }}
(10) PSI West is {{ .PSI24.West }}`

func main() {
	http.HandleFunc("/api/psi", psiAPIHandler)
	fmt.Println("starting HTTP server")
	http.HandleFunc("/api/now", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, time.Now().Format("Mon 2006-Jan-04 10:30"))
	})

	log.Fatal(http.ListenAndServe(":8080", nil))

}

func psiAPIHandler(w http.ResponseWriter, r *http.Request) {
	resp, err := http.Get("https://api.data.gov.sg/v1//environment/psi")
	if err != nil {
		panic(err)
	}

	respBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()

	parsedResp := PSIApiResp{}
	err = json.Unmarshal(respBytes, &parsedResp)
	if err != nil {
		panic(err)
	}

	psi24 := parsedResp.Items[0].Readings.PsiTwentyFourHourly
	pm10 := parsedResp.Items[0].Readings.Pm10TwentyFourHourly

	// fmt.Fprintf(w, "PSI 24H - National: %d, North: %d, East: %d, South: %d, West: %d\n",
	// 	psi24.National, psi24.North, psi24.East, psi24.South, psi24.West)

	// fmt.Fprintf(w, "<1> PM10 National is %d\n", pm10.National)
	// fmt.Fprintf(w, "<2> PM10 North is %d\n", pm10.North)
	// fmt.Fprintf(w, "<3> PM10 East is %d\n", pm10.East)
	// fmt.Fprintf(w, "<4> PM10 South is %d\n", pm10.South)
	// fmt.Fprintf(w, "<5> PM10 West is %d\n", pm10.West)

	// fmt.Fprintf(w, "<6> PSI National is %d\n", psi24.National)
	// fmt.Fprintf(w, "<7> PSI North is %d\n", psi24.North)
	// fmt.Fprintf(w, "<8> PSI East is %d\n", psi24.East)
	// fmt.Fprintf(w, "<9> PSI South is %d\n", psi24.South)
	// fmt.Fprintf(w, "<10> PSI West is %d\n", psi24.West)

	tmpl, err := template.New("respTmpl").Parse(respTmpl)
	if err != nil {
		panic(err)
	}

	err = tmpl.Execute(w, map[string]interface{}{
		"PSI24": psi24,
		"PM10":  pm10,
	})

	if err != nil {
		panic(err)
	}

}
